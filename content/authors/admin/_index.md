---
authors:
- admin
bio: My research interests include distributed robotics, mobile computing and programmable
  matter.
Educación:
  courses:
  - course: Estudiante de Medicina
    institution: FUCS
    year: ""

email: "gjespitia@fucsalud.edu.co"
Intereses:
- Emerging Systems
- Medicine
- Statistics and Data Science
- Genética
name: G. Espitia
organizations:
- name: Fundación Universitaria de Ciencias de la Salud (FUCS)
  url: "https://www.fucsalud.edu.co/"
role: Med. Student
social:
- icon: envelope
  icon_pack: fas
  link: '#contact'
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/Stradichenko
- icon: google-scholar
  icon_pack: ai
  link: https://scholar.google.co.uk/citations?user=sIwtMXoAAAAJ
- icon: github
  icon_pack: fab
  link: https://github.com/Stradichenko
superuser: true
user_groups:
- Researchers
- Visitors
---

Estudiante de Medicina de la Fundación Universitaria de Ciencias de la Salud (FUCS). Actualmente buscando crear un espacio para compartir y mejorar la divulgación del conocimiento médico a través de plataformas virtuales. Miembro del Semillero de Neurociencias de la FUCS. Interesado en R y paquetes de publicación como Blogdown.
